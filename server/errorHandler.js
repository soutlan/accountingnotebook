module.exports = (err, req, res, next) => {
	if (err.httpStatus) {
		res.status(err.httpStatus).send(err.error);
	} else {
		console.log(err);
		res.status(500).send('Something went wrong');
	}
};