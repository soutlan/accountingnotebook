const ReadWriteLock = require('rwlock');
const uuid = require('uuid/v1');
const Joi = require('joi');
const Transaction = require('../models/Transaction');
const Store = require('../store');
const { TransactionErrors, TransactionSchema } = require('../models/transaction.schema');
const lock = new ReadWriteLock();

const supportedOperations = {
	credit: (balance, amount) => balance - amount,
	debit: (balance, amount) => balance + amount
}

exports.getList = () => {
	return {
		error: null,
		balance: Store.balance().toFixed(2),
		transactions: Store.transactions()
	};
};

exports.add = (data) => {

	const result = {
		error: null
	};

	Joi.validate(data, TransactionSchema, (err, data) => {
		if (err) {
			result.error = TransactionErrors.INVALID_FORMAT;
		} else {
			// Using lock here is unnesesary due to Node.js single threaded nature and there is no I/O operations or timers.
			// But if we change store implementation (file, DB, remove server) then write lock is a must.
			lock.writeLock(release => {
				const currentBallance = Store.balance();
				const operation = supportedOperations[data.type];
				
				const newBalance = operation(currentBallance, data.amount);
			
				if (newBalance < 0) {
					result.error = TransactionErrors.NO_NEGATIVE_BALLANCE;
				} else {
					const transaction = new Transaction({
						id: uuid(),
						effectiveDate: (new Date()).toISOString(),
						amount: Number(data.amount).toFixed(2),
						type: data.type
					});

					Store.balance(newBalance);
					Store.log(transaction);

					result.transaction = transaction;
				}

				release();
			});
		}
	});

	return result;

};
