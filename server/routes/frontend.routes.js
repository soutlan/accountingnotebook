const express = require('express');

module.exports = app => {
	if (process.env.NODE_ENV === 'production') {
		app.use(express.static('server/client_build'));
		const path = require('path');
		app.get('*', (req, res) => {
			res.sendFile(
				path.resolve(__dirname, 'server', 'client_build', 'index.html')
			);
		});
	}
};
