const controller = require('../controllers/transations.controller');

module.exports = app => {
	app.get('/api/transactions', controller.getTransactionsList);
	app.post('/api/transactions', controller.addTransaction);
};