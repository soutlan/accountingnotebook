const TransactionService = require('../services/transactions.service');
const { TransactionErrors } = require('../models/transaction.schema');

exports.getTransactionsList = (req, res, next) => {	
	res.send(TransactionService.getList());
};

exports.addTransaction = (req, res, next) => {
	const result = TransactionService.add(req.body);

	if (result.error) {
		switch (result.error.code) {
			case TransactionErrors.NO_NEGATIVE_BALLANCE.code:
				result.httpStatus = 422;
				break;
			default:
				result.httpStatus = 400;
				break;
		}
		
		next(result);
	} else {
		res.send(result);
	}
};
