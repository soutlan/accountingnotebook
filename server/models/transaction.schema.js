const Joi = require('joi');

const TransactionTypes = { 
	credit: 'credit',
	debit: 'debit'
};

const TransactionSchema = {
	type: Joi.string().required().valid(...Object.values(TransactionTypes)),
	amount: Joi.number().required()
};

const TransactionErrors = {
	NO_NEGATIVE_BALLANCE: {
		code: 1001,
		details: 'Transaction refused: account ballance must be positive'
	},
	INVALID_FORMAT: {
		code: 1002,
		details: 'Invalid format'
	},
	UNKNOWN_ERROR: {
		code: 1003,
		details: 'Something went wrong.'
	}
};

exports.TransactionSchema = TransactionSchema;
exports.TransactionTypes = TransactionTypes;
exports.TransactionErrors = TransactionErrors;
