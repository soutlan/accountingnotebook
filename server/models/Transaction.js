module.exports = class Transaction {

	constructor(transaction) {
		this.id = transaction.id;
		this.type = transaction.type;
		this.amount = transaction.amount;
		this.effectiveDate = transaction.effectiveDate;
	}
};