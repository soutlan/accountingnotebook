const initialStore = () => ({
	transactions: [],
	ballance: 0
});

class Store {
	
	constructor() {
		this.store = initialStore();
	}

	balance(newBallance) {
		if (typeof newBallance === 'number') {
			this.store.ballance = newBallance;
		}
		return this.store.ballance;
	}

	transactions() {
		return this.store.transactions;
	}
	
	log(transaction) {
		this.store.transactions.push(transaction);
	}
} 

module.exports = new Store();