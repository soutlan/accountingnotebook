import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import TransactionItem from './TransactionItem';
import Money from '../common/Money';
import './TransactionsList.css';

class TransactionsList extends Component {

	state = {
		selectedId: ''
	}

	componentDidMount() {
		this.props.getTransactions();
	}

	toggleClick(transaction) {
		this.setState(state => {
			state.selectedId = transaction.id;
			return state;
		});
	}

	renderList() {
		return <ul className="transactions-list">{this.props.transactions.map(transaction => {
				return <li onClick={() => this.toggleClick(transaction)}
				key={transaction.id}>
				<TransactionItem 
					{...transaction}
					expanded={transaction.id === this.state.selectedId}
				/>
			</li>
		})}</ul>;
	}

	render() {
		if (this.props.loaded) {

		return <div>
		<h3>Balance: {this.props.balance}</h3>
		{(this.props.transactions.length > 0 ? this.renderList() : <div>No transactions so far</div>)}</div>;
		}
		return <div>Loading...</div>
	}
}



export default connect(
	state => ({
		transactions: state.transactions.list,
		balance: state.transactions.balance,
		loaded: state.transactions.loaded
	}), 
	actions
)(TransactionsList)
