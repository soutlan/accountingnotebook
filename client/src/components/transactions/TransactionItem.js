import React from 'react';
import Money from '../common/Money';

export default (props) => {

	const effectiveDate = new Date(props.effectiveDate);

	const classes = ['transactions-list-item', props.type];
	if (props.expanded) {
		classes.push('expanded');
	}
	return (
		<div className={classes.join(' ')}>
			<div className="left">
				<div className="date">
					<label>Effective Date</label>
					<span>{effectiveDate.toLocaleString()}</span>
				</div>
				{
					props.expanded ?
						<div className="id">
							<label>Transaction ID</label>
							<span>{props.id}</span>
						</div> :
						null
				}
			</div>
			<div  className="right amount" >
				<Money amount={props.amount} />
			</div>
		</div>
	);
}