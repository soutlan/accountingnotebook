import axios from 'axios';
import { GET_TRANSACTIONS } from './types';

export const getTransactions = () => async dispatch => {
	const res = await axios.get('/api/transactions');
	dispatch({
		type: GET_TRANSACTIONS,
		payload: { 
			balance: res.data.balance,
			list: res.data.transactions
		}
	});
};
