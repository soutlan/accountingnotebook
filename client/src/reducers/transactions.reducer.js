import { GET_TRANSACTIONS } from '../actions/types';

const initialState = {
	balance: 0,
	list: [],
	loaded: false
};
export default function(state = initialState, action) {
	switch (action.type) {
		case GET_TRANSACTIONS:
			const nextState = {
				...action.payload,
				loaded: true
			};
			return nextState;
		default:
			return state;
	}
}
