
# Development

## Installation
```sh
#server
npm install

#client
cd client
npm install
```

## Run


```sh
#Run server 
npm start

#and then client
npm run client
```

# Production

## Build

Build client with 
```sh
cd client
npm run build
```
Then copy 
`client\build`
to 
`server\client_build`. 

And run
```sh
npm pack
```

## Unpack archive and run in production
```sh
npm install accountingapp-1.0.0.tgz
cd node_modules\accountingapp
npm run start-prod
```