const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('./server/errorHandler');

const app = express();
app.use(bodyParser.json());

require('./server/routes/transactions.routes')(app);
require('./server/routes/frontend.routes')(app);

app.use(errorHandler);

const server = app.listen(process.env.PORT || 5005, () => {
	console.log('API is running', server.address());
	console.log('NODE_ENV', process.env.NODE_ENV);
});